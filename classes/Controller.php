<?php

require_once "Backpack.php";

class BackpackController
{
    private $totalWeight;
    private $totalValue;
    private $totalItems= [];

    public static function getTotalWeight()
    {

    }
    public static function getTotalValue()
    {

    }
    public static function getTotalItems()
    {

        foreach ($this->totalItems as $item) {
            echo '<li>' . $item . '</li>';
        }
    }

    /*
     * Checking conection and chose method of resolving problem
     */
    public static function UploadData()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST'){
            if(isset($_POST['maxWeight']) && isset($_POST['file'])){
                $method= $_POST['method'];
                if ($method == 'dynamic') {
                    self::showDynamicSolution();
                }
            } else {
                echo "There is lack of data or some of them are wrong";
            }
        }
    }

    /*
     * Using dynamic method to solve the problem
     */
    public static function showDynamicSolution()
    {
        $file= $_POST['file'];
        $maxWeight= $_POST['maxWeight'];
        $goods= new Backpack($file, $maxWeight);
        $file= $_POST['file'];
        $maxWeight= $_POST['maxWeight'];
        $goods= new Backpack($file, $maxWeight);
        echo '<p>' . 'Value of packed items: ' . $goods->showValue() . '</p>';
        echo '<hr>';
        echo '<p>' . "Weight of packed items: " . $goods->showWeight() . '</p>';
        echo '<hr>';
        echo '<p>' . "Items that have been packed to Knapsack: " . '<ul>';
        foreach ($goods->showIds() as $item) {
            echo '<li>' . $item . '</li>';
        }
    }
}
